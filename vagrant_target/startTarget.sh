ssh-keygen -f "/home/oose/.ssh/known_hosts" -R "192.168.33.11"
cp -f ~/.ssh/id_rsa.pub share/
vagrant up

for domain in "192.168.33.11"; do
    sed -i "/$domain/d" ~/.ssh/known_hosts
    line=$(ssh-keyscan $domain,`nslookup $domain | awk '/^Address: / { print $2 ; exit }'`)
    echo $line >> ~/.ssh/known_hosts
done
