#!/usr/bin/env bash


#####################################################
# update packages and upgrade base image	
#####################################################

apt update
apt -f -y install

#####################################################
# install ssh key 
#####################################################

mkdir /home/vagrant/.ssh || true
cat /mnt/share/id_rsa.pub >> /home/vagrant/.ssh/authorized_keys

chmod 0700 /home/vagrant/.ssh
chmod 0600 /home/vagrant/.ssh/authorized_keys
chown -R vagrant:vagrant /home/vagrant/.ssh


sed -ie 's/^PasswordAuthentication no/PasswordAuthentication yes/' /etc/ssh/sshd_config
systemctl restart sshd

#####################################################
# python for ansible 
#####################################################

apt-get purge python2.7-minimal
DEBIAN_FRONTEND=noninteractive apt-get -yq install python3


