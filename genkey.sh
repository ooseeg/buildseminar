#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )


mkdir /etc/docker
mkdir /etc/docker/tls
mkdir /etc/systemd/system/docker.service.d

cp $SCRIPT_DIR/docker.conf /etc/systemd/system/docker.service.d/
systemctl daemon-reload

cd /etc/docker/tls/

HOST=${1:-`hostname`}


mkdir client
rm -rf ca-key.pem ca.pem ca.srl server-cert.pem client.csr extfile.cnf server-key.pem key.pem cert.pem client/cert.pem client/key.pem client/ca.pem

# ca key
openssl genrsa -aes256 -out ca-key.pem  -passout pass:oose 4096

# ca certificate
openssl req -new -x509 -days 365 -key ca-key.pem -sha256 -out ca.pem -subj "/C=DE/ST=Hamburg/L=Hamburg/O=oose/OU=swat/CN=$HOST" -passin pass:oose


# server key
openssl genrsa -out server-key.pem 4096
# request signature by ca
openssl req -subj "/CN=$HOST" -sha256 -new -key server-key.pem -out server.csr
# the ips that can be used for connection

ALT_NAMES=""
for ip in $(hostname -I); do
   if [$ALT_NAMES eq ""]; then
     ALT_NAMES=IP:$ip
   else
     ALT_NAMES=$ALT_NAMES,IP:$ip
   fi
done
ALT_NAMES=$ALT_NAMES,DNS:$HOST


echo subjectAltName = $ALT_NAMES > extfile.cnf

# sign
openssl x509 -req -days 365 -sha256 -in server.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out server-cert.pem -extfile extfile.cnf -passin pass:oose



# client key
openssl genrsa -out client/key.pem 4096
# request signature by ca
openssl req -subj '/CN=client' -new -key client/key.pem -out client.csr
# key is to be used for client authentication
echo extendedKeyUsage = clientAuth > extfile.cnf

# sign
openssl x509 -req -days 365 -sha256 -in client.csr -CA ca.pem -CAkey ca-key.pem \
  -CAcreateserial -out client/cert.pem -extfile extfile.cnf  -passin pass:oose

# copy public ca cert for client
cp ca.pem client/

rm -v client.csr server.csr


chmod a+r client/*
chown root:docker client/*

# private keys
chmod -v 0440 ca-key.pem server-key.pem
# public certificates
chmod -v 0444 ca.pem server-cert.pem



# configure docker "client"
mkdir -pv /home/oose/.docker
cp -f client/* /home/oose/.docker/

service docker restart



