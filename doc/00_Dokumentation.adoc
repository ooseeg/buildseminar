= Architektur-Dokumentation
:sectids:
:numbered:
:toc:


include::01_Intro.adoc[]

include::02_Constraints.adoc[]

include::03_Context.adoc[]

include::04_Approach.adoc[]

include::05_Building_Blocks.adoc[]

include::06_Runtime.adoc[]

include::07_Deployment.adoc[]

include::08_Cross_Cutting_Concerns.adoc[]

include::09_Decisions.adoc[]

include::10_Quality.adoc[]

include::11_Risks_Debt.adoc[]

include::12_Glossary.adoc[]

include::99_Copyright.adoc[]

