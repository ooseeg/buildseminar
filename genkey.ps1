$ErrorActionPreference = "Stop"
if ([int](Get-ItemProperty "HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full"  -Name Release).Release -lt 393295) {
    throw "Your version of .NET framework is not supported for this script, needs at least 4.6+"
}

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# !! Das funktioniert noch nicht, wegen Limitationen im Windows
# !! Docker Daemon oder meines Unvermögens. 

function GenerateCerts {
    
    New-item "C:\test" -ItemType Directory -force    
    New-item "C:\test\client" -ItemType Directory -force

    $splat = @{
        type = "Custom" ;
        KeyExportPolicy = "Exportable";
        Subject = "CN=Docker TLS Root";
        CertStoreLocation = "Cert:\CurrentUser\My";
        HashAlgorithm = "sha256";
        KeyLength = 4096;
        KeyUsage = @("CertSign", "CRLSign");
        TextExtension = @("2.5.29.19 ={critical} {text}ca=1")
    }
    $rootCert = New-SelfSignedCertificate @splat
    $splat = @{
        Path = "c:\test\ca.pem";
        Value = "-----BEGIN CERTIFICATE-----`n" + [System.Convert]::ToBase64String($rootCert.RawData, [System.Base64FormattingOptions]::InsertLineBreaks) + "`n-----END CERTIFICATE-----";
        Encoding = "ASCII";
    }
    Set-Content @splat
    
    Copy-Item "c:\test\ca.pem" -Destination "c:\test\client"
            
    $splat = @{
        CertStoreLocation = "Cert:\CurrentUser\My";
        DnsName = "localhost", (hostname);
        Signer = $rootCert ;
        KeyExportPolicy = "Exportable";
        Provider = "Microsoft Enhanced Cryptographic Provider v1.0";
        Type = "SSLServerAuthentication";
        HashAlgorithm = "sha256";
        TextExtension = @("2.5.29.37= {text}1.3.6.1.5.5.7.3.1");
        KeyLength = 4096;
    }
    $serverCert = New-SelfSignedCertificate @splat
    $splat = @{
        Path = "c:\test\server-cert.pem";
        Value = "-----BEGIN CERTIFICATE-----`n" + [System.Convert]::ToBase64String($serverCert.RawData, [System.Base64FormattingOptions]::InsertLineBreaks) + "`n-----END CERTIFICATE-----";
        Encoding = "Ascii"
    }
    Set-Content @splat
 
    $privateKeyFromCert = [System.Security.Cryptography.X509Certificates.RSACertificateExtensions]::GetRSAPrivateKey($serverCert)
    $splat = @{
        Path = "c:\test\server-key.pem";
        Value = ("-----BEGIN RSA PRIVATE KEY-----`n" + [System.Convert]::ToBase64String($privateKeyFromCert.Key.Export([System.Security.Cryptography.CngKeyBlobFormat]::Pkcs8PrivateBlob), [System.Base64FormattingOptions]::InsertLineBreaks) + "`n-----END RSA PRIVATE KEY-----");
        Encoding = "Ascii";
    }
    Set-Content @splat
 
    $splat = @{
        CertStoreLocation = "Cert:\CurrentUser\My";
        Subject = "CN=clientCert";
        Signer = $rootCert ;
        KeyExportPolicy = "Exportable";
        Provider = "Microsoft Enhanced Cryptographic Provider v1.0";
        TextExtension = @("2.5.29.37= {text}1.3.6.1.5.5.7.3.2") ;
        HashAlgorithm = "sha256";
        KeyLength = 4096;
    }
    $clientCert = New-SelfSignedCertificate  @splat
    $splat = @{
        Path = "c:\test\client\cert.pem" ;
        Value = ("-----BEGIN CERTIFICATE-----`n" + [System.Convert]::ToBase64String($clientCert.RawData, [System.Base64FormattingOptions]::InsertLineBreaks) + "`n-----END CERTIFICATE-----");
        Encoding = "Ascii";
    }
    Set-Content  @splat
    $clientprivateKeyFromCert = [System.Security.Cryptography.X509Certificates.RSACertificateExtensions]::GetRSAPrivateKey($clientCert)
    $splat = @{
        Path = "c:\test\client\key.pem";
        Value = ("-----BEGIN RSA PRIVATE KEY-----`n" + [System.Convert]::ToBase64String($clientprivateKeyFromCert.Key.Export([System.Security.Cryptography.CngKeyBlobFormat]::Pkcs8PrivateBlob), [System.Base64FormattingOptions]::InsertLineBreaks) + "`n-----END RSA PRIVATE KEY-----");
        Encoding = "Ascii";
    }
    Set-Content  @splat
}

GenerateCerts


New-item "C:\ProgramData\docker\certs.d" -ItemType Directory -force
Copy-Item "C:\test\ca.pem" -Destination "C:\ProgramData\docker\certs.d"
Copy-Item "C:\test\server-cert.pem" -Destination "C:\ProgramData\docker\certs.d"
Copy-Item "C:\test\server-key.pem" -Destination "C:\ProgramData\docker\certs.d"


New-item "C:\ProgramData\docker\config" -ItemType Directory -force
$config=@"
{
"hosts":  ["tcp://0.0.0.0:2376", "npipe://"],
"tlsverify":  true,
"tlscacert":  "C:\\ProgramData\\docker\\certs.d\\ca.pem",
"tlscert":  "C:\\ProgramData\\docker\\certs.d\\server-cert.pem",
"tlskey":  "C:\\ProgramData\\docker\\certs.d\\server-key.pem"
}
"@


Out-File -FilePath C:\ProgramData\docker\config\daemon.json -InputObject $config


# Restart-Service dockerd
 