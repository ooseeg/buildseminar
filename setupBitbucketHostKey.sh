#!/bin/bash


for domain in "github.com" "bitbucket.org"; do
    sed -i "/$domain/d" ~/.ssh/known_hosts
    line=$(ssh-keyscan $domain,`nslookup $domain | awk '/^Address: / { print $2 ; exit }'`)
    echo $line >> ~/.ssh/known_hosts
done
