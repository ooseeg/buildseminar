
# Set-ExecutionPolicy -ExecutionPolicy RemoteSigned

$env:HOSTNAME = ((hostname) | Out-String).Replace("`n","").Replace("`r","")
$env:IP = (([System.Net.Dns]::GetHostByName($HOSTNAME).AddressList[0].IPAddressToString) | Out-String).Replace("`n","").Replace("`r","")
$env:HOME = "~"
$env:TLS_VERIFY = ""
$env:DOCKER_PORT = "2375"
$env:TLS_DIR = "C:\test\client\"

# Run as admin to allow remote access to insecure docker port
netsh interface portproxy add v4tov4 listenport=2375 listenaddress=$env:IP connectport=2375 connectaddress=127.0.0.1

docker compose $args[0] 

# docker exec --user root -it compose_jenkins_jenkins_1 chown jenkins:jenkins /var/jenkins_home/workspace/
