#!/bin/sh

mkdir ${HOME}/jenkins_home
chmod a+rwx ${HOME}/jenkins_home

export HOSTNAME=`hostname`
export IP=`hostname -I | cut -d ' ' -f 1`

export DOCKER_PORT=2376
export TLS_VERIFY=1
export TLS_DIR=/etc/docker/tls/client

exec docker-compose $@ 

# exec docker exec --user root -it compose_jenkins_jenkins_1 chown jenkins:jenkins /var/jenkins_home/workspace/
