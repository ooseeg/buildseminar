package de.oose.build.event;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

public class EventTest {

	@Test
	public void thereIsNothingToTest() throws Exception {
		Event event = EventCreator.createValidEvent();

		assertNotNull(event.getTitle());
	}
}
