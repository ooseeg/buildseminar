package de.oose.build.event;

import java.time.LocalDate;

public class EventCreator {

	public static Event createValidEvent() {
		Event event = new Event();
		event.setTitle("toll");
		event.setDescription("toll");
		event.setDate(LocalDate.now());
		return event;
	}
}
