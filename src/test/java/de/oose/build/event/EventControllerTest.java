package de.oose.build.event;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.ui.Model;

import io.micrometer.core.instrument.MeterRegistry;

@ExtendWith(MockitoExtension.class)
public class EventControllerTest {

	@Mock
	private EventRepository repository;

	@Mock
	private MeterRegistry registry;

	@Mock
	private Model model;

	private EventController eventController;

	@InjectMocks
	private EventService service;

	@Captor
	private ArgumentCaptor<Event> eventCaptor;

	private UserDetails user = new User("Felix", "x", Collections.emptySet());

	private Event simpleEvent;

	@BeforeEach
	public void setUp() {
		when(registry.counter(any(String.class))).thenReturn(mock(io.micrometer.core.instrument.Counter.class));

		LocalDate date = LocalDate.of(2018, 1, 4);
		String eventName = "Barcamp";
		simpleEvent = new Event();
		simpleEvent.setTitle(eventName);
		simpleEvent.setDate(date);

		eventController = new EventController(service, registry);

	}

	@Test
	public void shouldSaveEventAndAddPrincipleAsParticipant_whenNewEventAdded() throws Exception {
		when(repository.save(any(Event.class))).thenReturn(simpleEvent);

		eventController.addEvent(simpleEvent, user, model);

		verify(repository).save(eventCaptor.capture());
		assertEquals(simpleEvent.getTitle(), eventCaptor.getValue().getTitle());
	}

}
