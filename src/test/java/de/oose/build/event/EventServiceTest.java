package de.oose.build.event;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class EventServiceTest {

	@Mock
	private EventRepository eventRepository;

	@InjectMocks
	private EventService eventService;

	private Event simpleEvent;

	@BeforeEach
	public void setUp() {
		LocalDate date = LocalDate.of(2018, 1, 4);
		String eventName = "Barcamp";
		simpleEvent = new Event();
		simpleEvent.setTitle(eventName);
		simpleEvent.setDate(date);
	}

	@Test
	public void shouldAddRoomToEvent_whenNewRoomAdded() throws Exception {

		when(eventRepository.findById(simpleEvent.getId())).thenReturn(Optional.of(simpleEvent));

		Optional<Event> findEventById = eventService.findEventById(simpleEvent.getId());

		assertSame(simpleEvent, findEventById.get());
	}

}
