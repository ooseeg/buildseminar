package de.oose.build;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.all;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static de.oose.build.ArchUnitUtil.arePublic;
import static de.oose.build.ArchUnitUtil.methods;
import static de.oose.build.ArchUnitUtil.returnType;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaMember;
import com.tngtech.archunit.core.domain.properties.HasOwner;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchRule;

//@Ignore("experimental")
public class ControllerPatternTest {
	private JavaClasses classes;

	@BeforeEach
	public void setup() {
		classes = new ClassFileImporter().importPackages("de.oose");
	}

	@Test
	public void controllers_mustBeAnnotatedAsRestControllers() {
		ArchRule rule = classes().that().haveSimpleNameEndingWith("Controller").should()
				.beAnnotatedWith(Controller.class).because("¯\\_(ツ)_/¯");
		rule.check(classes);
	}

	@Test
	public void publicMethodsInControllers_shouldReturnResponseEntities() {
		all(methods()).that(areDefinedInAController()).and(arePublic()).should(returnType(ResponseEntity.class))
				.because("it forces us to spend more time thinking about how our public interface looks,"
						+ "PLUS we're not relying a little less on spring magic");
	}

	private static DescribedPredicate<? super JavaMember> areDefinedInAController() {
		return HasOwner.Functions.Get.<JavaClass>owner().is(JavaClass.Predicates.simpleNameEndingWith("Controller"));
	}

}
