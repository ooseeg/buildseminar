package de.oose.build;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchRule;

public class ServicePatternTest {
	private JavaClasses classes;

	@BeforeEach
	public void setup() {
		classes = new ClassFileImporter().importPackages("de.oose");
	}

	@Test
	public void services_mustNotKnow_controllers() {
		ArchRule rule = noClasses().that().haveSimpleNameEndingWith("Service").should().dependOnClassesThat()
				.areAnnotatedWith(Controller.class).because("controllers know services and not the other way around");
		rule.check(classes);
	}

	@Test
	public void services_mustBeAnnotatedAsSuch() {
		ArchRule rule = classes().that().haveSimpleNameEndingWith("Service").and().resideOutsideOfPackage("..config..")
				.should().beAnnotatedWith(Service.class).because("¯\\_(ツ)_/¯");
		rule.check(classes);
	}
}
