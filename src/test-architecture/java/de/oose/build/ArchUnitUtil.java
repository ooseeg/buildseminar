package de.oose.build;

import static com.tngtech.archunit.core.domain.JavaModifier.PUBLIC;
import static com.tngtech.archunit.core.domain.properties.HasModifiers.Predicates.modifier;

import java.util.ArrayList;
import java.util.List;

import com.tngtech.archunit.base.DescribedPredicate;
import com.tngtech.archunit.core.domain.JavaClass;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.domain.JavaMethod;
import com.tngtech.archunit.core.domain.properties.HasModifiers;
import com.tngtech.archunit.lang.AbstractClassesTransformer;
import com.tngtech.archunit.lang.ArchCondition;
import com.tngtech.archunit.lang.ClassesTransformer;
import com.tngtech.archunit.lang.ConditionEvents;
import com.tngtech.archunit.lang.SimpleConditionEvent;

public class ArchUnitUtil {

	public static ClassesTransformer<JavaMethod> methods() {
		return new AbstractClassesTransformer<JavaMethod>("methods") {
			@Override
			public Iterable<JavaMethod> doTransform(JavaClasses javaClasses) {
				List<JavaMethod> methods = new ArrayList<>();
				for (JavaClass javaClass : javaClasses) {
					methods.addAll(javaClass.getMethods());
				}
				return methods;
			}
		};
	}

	public static DescribedPredicate<HasModifiers> arePublic() {
		return modifier(PUBLIC).as("are public");
	}

	public static ArchCondition<JavaMethod> returnType(final Class<?> type) {
		return new ArchCondition<JavaMethod>("return type " + type.getName()) {
			@Override
			public void check(JavaMethod method, ConditionEvents events) {
				boolean typeMatches = method.getRawReturnType().isAssignableTo(type);
				String message = String.format("%s returns %s in %s", method.getFullName(),
						method.getRawReturnType().getName(), method.getOwner().getName());
				events.add(new SimpleConditionEvent(method, typeMatches, message));
			}
		};
	}
}
