package de.oose.build;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.lang.ArchRule;

public class DependenciesTest {

	private JavaClasses classes;

	@BeforeEach
	public void setup() {
		classes = new ClassFileImporter().importPackages("de.oose");
	}

	@Test
	public void backlog_mustNotKnow_anyone() {
		ArchRule rule = noClasses().that().resideInAPackage("..backlog..").should().dependOnClassesThat()
				.resideInAnyPackage("..agenda..", "..event..", "..scheduler..")
				.because("the backlog should be self contained, maybe it will be run as a separate service later");
		rule.check(classes);
	}

	@Test
	public void schedule_onlyKnows_agenda() {
		ArchRule rule = noClasses().that().resideInAPackage("..scheduler..").should().onlyDependOnClassesThat()
				.resideInAPackage("..agenda..").because("the should only be concerned with arranging sessions");
		rule.check(classes);
	}

	@Test
	public void agenda_onlyKnows_backlog() {
		ArchRule rule = noClasses().that().resideInAPackage("..agenda..").should().onlyDependOnClassesThat()
				.resideInAPackage("..backlog..").because("while a session's topic has to be known in the client, "
						+ "everything else can and should be done using ids only");
		rule.check(classes);
	}

	@Test
	public void event_shouldBeSelfContained() {
		ArchRule rule = noClasses().that().resideInAPackage("..event..").should().dependOnClassesThat()
				.resideInAnyPackage("..agenda..", "..backlog..", "..scheduler..")
				.because("high level modules should not depend on low level modules");
		rule.check(classes);
	}
}
