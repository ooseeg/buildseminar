#!/usr/bin/env bash

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

sed -i \\'s/\\(ansible_ssh_host=\\)[^\\s]*/\\1$(cat $SCRIPTPATH/IP.txt) /g\\' $SCRIPTPATH/hosts