package de.oose.build.event;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EventService {

	@Autowired
	private EventRepository repository;

	public Event saveEvent(Event event) {
		return repository.save(event);
	}

	public void deleteEventById(Long eventId) {
		repository.deleteById(eventId);
	}

	public Optional<Event> findEventById(Long id) {
		return repository.findById(id);
	}

	public Iterable<Event> findAllEvents() {
		return repository.findAll();
	}

}