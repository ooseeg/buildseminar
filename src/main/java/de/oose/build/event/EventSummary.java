package de.oose.build.event;

import java.time.LocalDate;

public class EventSummary {

	private Long id;

	private String title;
	
//	private String type;

	private LocalDate date;

	public EventSummary(Event e) {
		this.id = e.getId();
		this.title = e.getTitle();
//		this.type = e.getType();
		this.date = e.getDate();
	}

	public Long getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}
	
//	public String getType() {
//		return type;
//	}

	public LocalDate getDate() {
		return date;
	}

}
