package de.oose.build.event;

import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<Event, Long> {

	Iterable<Event> findByTitle(String title);

}
