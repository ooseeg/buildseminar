package de.oose.build.event;

import java.util.List;
import java.util.Optional;
import java.util.Spliterator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

@Controller
@Transactional
public class EventController {

	private EventService service;
	private final Counter counter;

	@Autowired
	public EventController(EventService service, MeterRegistry registry) {
		this.service = service;
		this.counter = registry.counter("build.newEvents");
	}

	@GetMapping("/addEvent")
	public String addEvent(Model model) {

		model.addAttribute("event", new Event());

		return "addEventView";
	}

	@PostMapping("/addEvent")
	public String addEvent(@ModelAttribute Event event, @AuthenticationPrincipal UserDetails user, Model model) {

		model.addAttribute("event", service.saveEvent(event));
		counter.increment();
		return "eventView";
	}

	@GetMapping("/event/{id}")
	public String findById(@PathVariable Long id, Model model) {
		Optional<Event> event = service.findEventById(id);

		model.addAttribute("event", event.orElse(null));
		return "eventView";
	}

	@GetMapping("/events")
	public String findAll(Model model) {
		Spliterator<Event> eventSpliterator = service.findAllEvents().spliterator();
		List<EventSummary> events = StreamSupport.stream(eventSpliterator, false).map((e) -> new EventSummary(e))
				.collect(Collectors.toList());
		model.addAttribute("events", events);

		return "eventsView";
	}

}
