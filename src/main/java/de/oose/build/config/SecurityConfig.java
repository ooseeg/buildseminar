package de.oose.build.config;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.authorizeRequests().mvcMatchers("/css/**").permitAll().and().formLogin()
				.failureUrl("/login?error").permitAll().and().logout().permitAll().and().authorizeRequests()
				.mvcMatchers("/actuator/**").permitAll().and().authorizeRequests().mvcMatchers("/bar")
				.access("hasRole('ADMIN')").and().authorizeRequests().anyRequest().fullyAuthenticated();

	}
	
	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication().withUser("admin").password(passwordEncoder().encode("admin")).roles("ADMIN", "USER").and().withUser("user")
				.password(passwordEncoder().encode("user")).roles("USER").and().passwordEncoder(passwordEncoder());
	}

}
