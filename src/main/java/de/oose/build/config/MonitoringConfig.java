package de.oose.build.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.info.GitProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;

@Configuration
public class MonitoringConfig {

	@Autowired(required = false)
	private BuildProperties buildProperties;

	@Autowired(required = false)
	private GitProperties gitProperties;

	@Autowired
	private MeterRegistry registry;

	@Bean
	public Gauge buildInfoGauge() {

		if (buildProperties == null) {
			return Gauge.builder("openspace.build.info", this, (a) -> 1.0).description("build info about openspace")
					.register(registry);
		}

		return Gauge.builder("openspace.build.info", this, (a) -> 1.0).description("build info about openspace")
				.tag("version", buildProperties.getVersion()).tag("group", buildProperties.getGroup())
				.tag("name", buildProperties.getName()).tag("date", buildProperties.getTime().toString())
				.tag("commit", gitProperties != null ? gitProperties.getShortCommitId() : "na").register(registry);
	}

}
