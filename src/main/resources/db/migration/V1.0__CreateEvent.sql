CREATE TABLE event (
  id int NOT NULL,
  title varchar(255) DEFAULT NULL,
  date DATE DEFAULT NULL,
  description varchar(255) DEFAULT NULL,
  
  PRIMARY KEY (id)
);

create sequence hibernate_sequence;