outputPath = 'build/docs'
inputFiles = [[file: '*.adoc', formats: ['html']],]
taskInputsFiles = []

//confluence config:
input = [
        [ file: "build/docs/html5/00_Dokumentation.html", ancestorId: 160007700],
]

confluenceAPI = 'http://wiki.oosede.local:8090/rest/api/'
confluenceSpaceKey = 'BuildDemoSpace'
confluenceCreateSubpages = false
confluencePagePrefix = ''
confluenceCredentials = 'DiesIstEinGeheimesPasswort'.bytes.encodeBase64().toString()
extraPageContent = ''